import pandas as pd
import matplotlib.pyplot as plt
import re


def plot_pie(counts, labels, name, startangle=150):
    plt.pie(counts, labels=labels, autopct='%1.1f%%', startangle=startangle)
    plt.axis('equal')
    plt.savefig('/home/zhenya/Documents/magi/mimobog/vacancies/plots/{}.png'.format(name))
    # plt.show()


if __name__ == '__main__':
    data = pd.read_csv('output.csv')
    # print(data.region.unique())
    data.loc[data.region == '[RU:17:area.None]', 'region'] = 'Минск'
    grouped_by_region = data.loc[data.region.notnull(), ['region', 'title']].groupby('region').count().reset_index()
    data.salary = data.salary.str.split().str.join('')
    plot_pie(grouped_by_region.title.values, grouped_by_region.region.values)
    a =[int(''.join(re.findall(r'до(\d+)', salary))) for salary in data.loc[(data.min_salary.isnull()) & (data.salary != 'з/пнеуказана'), 'salary'].values]
    data.loc[(data.min_salary.isnull()) & (data.salary != 'з/пнеуказана'), 'max_salary'] = a
    # print(a)
    print(data.loc[(data.min_salary.isnull()) & (data.max_salary.isnull()), 'salary'].unique())
    # print(data.loc[(data.salary != 'з/пнеуказана' & data.min_salary.isnull()])
    # print(data.salary)
