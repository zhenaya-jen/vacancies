import unittest
import os
import pandas as pd
from main import plot_pie


class TestStringMethods(unittest.TestCase):

    def test_0(self):
        self.assertTrue(os.path.exists('/home/zhenya/Documents/magi/mimobog/vacancies/output.csv'))

    def test_1(self):
        data = pd.read_csv('/home/zhenya/Documents/magi/mimobog/vacancies/output.csv')
        data.loc[data.region == '[RU:17:area.None]', 'region'] = 'Минск'
        grouped_by_region = data.loc[data.region.notnull(), ['region', 'title']].groupby('region').count().reset_index()
        plot_pie(grouped_by_region.title.values, grouped_by_region.region.values, 'by_region')
        self.assertTrue(os.path.exists('/home/zhenya/Documents/magi/mimobog/vacancies/plots/by_region.png'))


  # def test_split(self):
  #     s = 'hello world'
  #     self.assertEqual(s.split(), ['hello', 'world'])
  #     # Проверим, что s.split не работает, если разделитель - не строка
  #     with self.assertRaises(TypeError):
  #         s.split(2)


if __name__ == '__main__':
    unittest.main()
