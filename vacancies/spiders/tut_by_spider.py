import scrapy
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError

from vacancies.items import VacanciesItem


class TutBySpider(scrapy.Spider):
    name = "tut_by"
    allowed_domains = ['jobs.tut.by']
    start_urls = ['https://jobs.tut.by/search/vacancy?currency_code=BYR&area=16']

    def start_requests(self):
        for u in self.start_urls:
            yield scrapy.Request(u, callback=self.parse,
                                 dont_filter=True)

    def parse(self, response):
        for href in response.css('div.resume-search-item__name a::attr(href)').getall():
            yield response.follow(href, self.parse_vacancy)  # {'href': href}
        for href in response.css('a.HH-Pager-Controls-Next::attr(href)'):
            yield response.follow(href, self.parse)

    def parse_vacancy(self, response):
        item = VacanciesItem()
        title_1 = ''.join(response.css('div.vacancy-title h1.header span::text').getall())
        title_2 = ''.join(response.css('div.vacancy-title h1.header::text').getall())
        item['title'] = title_1 if title_1 != '' else title_2
        item['salary'] = ''.join(response.css('p.vacancy-salary::text').getall())
        item['min_salary'] = ''.join(response.css('meta[itemprop="minValue"]::attr(content)').getall())
        item['max_salary'] = ''.join(response.css('meta[itemprop="maxValue"]::attr(content)').getall())
        item['experience'] = ''.join(response.css('span[data-qa="vacancy-experience"]::text').getall())
        item['employment_type'] = ''.join(response.css('meta[itemprop="employmentType"]::attr(content)').getall())
        item['work_hours'] = ''.join(response.css('span[itemprop="workHours"]::text').getall())
        item['region'] = ''.join(response.css('meta[itemprop="addressRegion"]::attr(content)').getall())
        # item['locality'] = ','.join(response.css('meta[itemprop="addressLocality"]::attr(content)').getall())
        # item['industry'] = ','.join(response.css('meta[itemprop="industry"]::attr(content)').getall())
        # item['company_name'] = ','.join(response.css('meta[itemprop="name"]::attr(content)').getall())
        # yield item
