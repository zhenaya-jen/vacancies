# -*- coding: utf-8 -*-
import scrapy


class VacanciesItem(scrapy.Item):
    title = scrapy.Field()
    salary = scrapy.Field()
    min_salary = scrapy.Field()
    max_salary = scrapy.Field()
    experience = scrapy.Field()
    employment_type = scrapy.Field()
    work_hours = scrapy.Field()
    region = scrapy.Field()
    locality = scrapy.Field()
    industry = scrapy.Field()
    company_name = scrapy.Field()
