import scrapy
from vacancies.items import VacanciesItem
from scrapytest.tests import Type, Required


class TestPost(scrapy.ItemSpec):
    item_cls = VacanciesItem

    title_test = Type(int), Required()
    salary_test = Type(int), Required()
    min_salary_test = Type(str)
    max_salary_test = Type(str)
    employment_type_test = Type(str), Required()
    work_hours_test = Type(str), Required()
    region_test = Type(str)
    locality_test = Type(str)
    industry_test = Type(str), Required()

    def url_test(self, value: str):
        if not value.startswith('https://jobs.tut.by'):
            return 'Invalid url: {}'.format(value)
        return ''
